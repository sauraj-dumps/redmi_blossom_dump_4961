## dandelion-user 11 RP1A.200720.011 V12.5.9.0.RCDINXM release-keys
- Manufacturer: xiaomi
- Platform: 
- Codename: blossom
- Brand: Redmi
- Flavor: arrow_blossom-userdebug
- Release Version: 13
- Kernel Version: 4.19.127
- Id: TQ2A.230505.002
- Incremental: eng.root.20230607.211718
- Tags: release-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/dandelion_in/dandelion:11/RP1A.200720.011/V12.5.9.0.RCDINXM:user/release-keys
- OTA version: 
- Branch: dandelion-user-11-RP1A.200720.011-V12.5.9.0.RCDINXM-release-keys
- Repo: redmi_blossom_dump_4961
